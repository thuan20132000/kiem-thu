@include('admin/layouts/header')
@include('admin/layouts/leftSide')

    <div class="wrapper">
        <div class="main-panel">
            @include('admin/layouts/navbar')
            @yield('content')
            @include('admin/layouts/footer')
        </div>
    </div>


