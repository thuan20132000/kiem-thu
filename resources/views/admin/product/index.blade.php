@extends('admin.layouts.master')

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
                <div class="col-md-6">
                   <button class="btn btn-danger" id='product-deleteAllCheckbox' >Delete Selected</button>
                </div>
                <div class="col-md-6 ">
                    <a class="btn btn-success float-right" href="{{ route('product.create') }}">
                    Add
                    </a>
                </div>
        </div>
        <div>
            @if(session()->has('success'))
            <div class="alert alert-success" >
                {{ session()->get('success') }}
            </div>
            @endif
            @if(session()->has('failed'))
            <div class="alert alert-success" >
                {{ session()->get('failed') }}
            </div>
            @endif
        </div>
      <div class="row">
        <div class="col-md-12">
          <div class="card">

            <div class="card-body">
              <div class="table-responsive">
                <table class="table">
                  <thead class=" text-primary">
                    <tr>
                    <th>
                        <input type="checkbox" name="" id="product-selectAllCheckbox">
                    </th>
                    <th>
                      ID
                    </th>
                    <th>
                      Name
                    </th>
                    <th>
                        Description
                    </th>
                    <th>
                        Stock
                    </th>
                    <th>
                        Price
                    </th>
                    <th>
                      Image
                    </th>
                    <th>
                        Setting
                    </th>
                  </tr></thead>
                  <tbody>

                    @foreach ($products as $key => $product)
                        <tr>
                            <td>
                                <input type="checkbox" name="product[]" id="" value="{{$product->id}}">
                            </td>
                            <td><p>{{$key+1}}</p></td>
                            <td><p>{{$product->name}}</p></td>
                            <td>
                                <p>{{$product->description}}</p>
                            </td>
                            <td>{{$product->stock}}</td>
                            <td>${{$product->price}}</td>
                            <td>
                                <img src="{{ asset('uploads/'.$product->image) }}" width="100" alt="" srcset="">
                            </td>
                            <td style="width: 20%">
                                <a href="{{ route('product.edit',$product->id) }}" class="btn btn-primary">
                                        <span class="material-icons">construction</span>
                                </a>
                                <button id="product-{{$product->id}}" onclick="confirmDelete({{$product->id}},'product')" data-id="{{$product->id}}" data-name="{{$product->name}}" class="btn btn-danger">
                                    <span class="material-icons">delete</span>
                                    <form action="{{ route('product.destroy',$product->id)}}" hidden method="POST">
                                        @csrf
                                        @method('DELETE')
                                    </form>
                                </button>

                            </td>
                        </tr>
                    @endforeach


                  </tbody>
                </table>
                <div style="display:flex;justify-content: center">
                    {{$products->links()}}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


@endsection
