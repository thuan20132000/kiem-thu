@extends('admin.layouts.master')
<link rel="stylesheet" href="{{ asset('assets/adminForm.css') }}">

@section('content')
<div class="content">
    <div class="kt--bg-header kt--pd-20">
            <h3>Product</h3>
    </div>
    <div class="kt--pd-100">
        <div>
            @if(session()->has('success'))
            <div class="alert alert-success" >
                {{ session()->get('success') }}
            </div>
            @endif
            @if(session()->has('failed'))
            <div class="alert alert-success" >
                {{ session()->get('failed') }}
            </div>
            @endif
        </div>
        <form method="POST" action="{{ route('product.update',$product)}}" enctype="multipart/form-data">
            @csrf
            @method('PUT')

            <div class="form-row kt--mgb-20">
                <div class="form-group col-md-4 is-focused">
                    <label for="">Category</label>
                    <div class="input-group mb-3">
                        <select id="category" name="category_id" class="custom-select form-control form-control @error('category_id') is-invalid @enderror" id="inputGroupSelect02">
                            <option  value="{{$product->producttype->category->id}}">{{$product->producttype->category->name}}</option>
                        @foreach ($categories as $category)
                            <option value="{{$category->id}}">{{$category->name}}</option>
                        @endforeach
                        </select>
                        @error('category_id')
                        <span class="error invalid-feedback">{{$message}}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group col-md-4 is-focused">
                    <label for="">Product Type</label>
                    <div class="input-group mb-3">
                        <select id="producttype" name="producttype_id" class="custom-select form-control form-control @error('producttype_id') is-invalid @enderror" id="inputGroupSelect02">
                        <option  value="{{$product->producttype->id}}">{{$product->producttype->name}}</option>

                        </select>
                        @error('producttype_id')
                        <span class="error invalid-feedback">{{$message}}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group col-md-4 is-focused">
                    <label for="">Vendor</label>

                    <div class="input-group mb-3">
                        <select id="vendor" name="vendor_id" class="custom-select form-control form-control @error('vendor_id') is-invalid @enderror" id="inputGroupSelect02">
                            <option  value="{{$product->vendor->id}}">{{$product->vendor->name}}</option>
                        @foreach ($vendors as $vendor)
                            <option value="{{$vendor->id}}">{{$vendor->name}}</option>
                          @endforeach

                        </select>
                        @error('vendor_id')
                        <span class="error invalid-feedback">{{$message}}</span>
                        @enderror
                    </div>
                </div>


            </div>


            <div class="form-row kt--mgb-20">
              <div class="form-group col-md-6 is-focused">
                <label for="inputName">Name</label>
                <input name="name" value="{{$product->name}}" type="text" class="form-control form-control @error('name') is-invalid @enderror" id="inputName">
                @error('name')
                    <span class="error invalid-feedback">{{$message}}</span>
                @enderror
              </div>
              <div class="form-group col-md-6 is-focused">
                <label for="inputStock">Stock</label>
                <input name="stock" value="{{$product->stock}}" type="text" class="form-control form-control @error('stock') is-invalid @enderror" id="inputStock">
                @error('stock')
                    <span class="error invalid-feedback">{{$message}}</span>
                @enderror
              </div>
            </div>
            <div class="form-row kt--mgb-20">
                <div class="form-group col-md-6 is-focused">
                  <label for="inputPrice">Price</label>
                  <input name="price" value="{{$product->price}}" type="text" class="form-control form-control @error('price') is-invalid @enderror" id="inputPrice">
                  @error('price')
                      <span class="error invalid-feedback">{{$message}}</span>
                  @enderror
                </div>


            </div>

            <div class="form-row kt--mgb-20">

                <div class="form-group col-md-12 is-focused">
                  <label for="inputDescription">Description</label>
                  <textarea  name="description" rows="6" class="form-control form-control @error('description') is-invalid @enderror" id="inputDescription">
                     {{$product->description}}
                  </textarea>
                  @error('description')
                      <span class="error invalid-feedback">{{$message}}</span>
                  @enderror
                </div>
            </div>


            <div class="custom-file kt--mgt-20" id="uploadForm">
                <input type="file" name="image"  id="file">
            </div>
            <img src="{{ asset('uploads/'.$product->image) }}" width="200px" alt="" srcset="">

            <div class="kt--mgt-20">
                <button type="submit" class="btn btn-primary float-right">Update</button>
            </div>

          </form>
    </div>

</div>


@endsection
