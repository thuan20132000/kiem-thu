@extends('admin.layouts.master')

@section('content')
<div class="content">
    <div class="container-fluid">

        <div>
            @if(session()->has('success'))
            <div class="alert alert-success" >
                {{ session()->get('success') }}
            </div>
            @endif
            @if(session()->has('failed'))
            <div class="alert alert-success" >
                {{ session()->get('failed') }}
            </div>
            @endif
        </div>


        <div class="row">
            <div class="col-md-6">
                <table class="table">

                    <tbody>

                        <tr>
                            <td class="text-center alert">Username</td>
                            <td> <span  class="btn btn-warning btn-round">Andrew Mike</span> </td>
                        </tr>
                        <tr>
                            <td class="text-center alert">Phone</td>
                            <td> <span  class="btn btn-warning btn-round">Andrew Mike</span> </td>
                        </tr>
                        <tr>
                            <td class="text-center alert">Email</td>
                            <td> <span  class="btn btn-warning btn-round">Andrew Mike</span> </td>
                        </tr>
                        <tr>
                            <td class="text-center alert">Address</td>
                            <td> <span  class="btn btn-warning btn-round">Andrew Mike</span> </td>
                        </tr>
                        <tr>
                            <td class="text-center alert">Country</td>
                            <td> <span  class="btn btn-warning btn-round">Andrew Mike</span> </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="col-md-6">
                <table class="table">

                    <tbody>
                        <tr>
                            <td class="text-center alert alert-info">Order Status</td>
                            <td>
                                <select class="btn-success" data-style="btn btn-primary btn-round" title="Single Select">
                                    <option disabled selected>Single Option</option>
                                    <option value="2">Foobar</option>
                                    <option value="3">Is great</option>
                                    <option value="4">Is awesome</option>
                                    <option value="5">Is wow</option>
                                    <option value="6">Boom !</option>
                                  </select>
                            </td>


                        </tr>
                        <tr>
                            <td class="text-center alert alert-info">Shipping Status</td>
                            <td>
                                <select class="btn-success" data-style="btn btn-primary btn-round" title="Single Select">
                                    <option disabled selected>Single Option</option>
                                    <option value="2">Foobar</option>
                                    <option value="3">Is great</option>
                                    <option value="4">Is awesome</option>
                                    <option value="5">Is wow</option>
                                    <option value="6">Boom !</option>
                                  </select>
                            </td>


                        </tr>
                        <tr>
                            <td class="text-center alert alert-info">Payment Status</td>
                            <td>
                                <select class="btn-success" data-style="btn btn-primary btn-round" title="Single Select">
                                    <option disabled selected>Single Option</option>
                                    <option value="2">Foobar</option>
                                    <option value="3">Is great</option>
                                    <option value="4">Is awesome</option>
                                    <option value="5">Is wow</option>
                                    <option value="6">Boom !</option>
                                  </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center alert alert-info">Payment Method</td>
                            <td>
                                    <select class="btn-success" data-style="btn btn-primary btn-round" title="Single Select">
                                      <option disabled selected>Single Option</option>
                                      <option value="2">Foobar</option>
                                      <option value="3">Is great</option>
                                      <option value="4">Is awesome</option>
                                      <option value="5">Is wow</option>
                                      <option value="6">Boom !</option>
                                    </select>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>


        </div>


        {{-- product order detail --}}
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table">
                  <thead class=" text-primary">
                    <tr>
                    <th>
                        <input type="checkbox" name="" id="orderDetail-selectAllCheckbox">
                    </th>
                    <th>
                      ID
                    </th>

                    <th>
                        Product
                    </th>
                    <th>
                        Image
                    </th>
                    <th>
                       Quantity
                    </th>
                    <th>
                       Price
                    </th>

                  </tr></thead>
                  <tbody>

                    @foreach ($orderDetails as $key => $orderDetail)
                        <tr>

                            <td><p>{{$key+1}}</p></td>
                            {{-- <td><p><a href="http://">{{$orderDetail->}}</a> </p></td> --}}
                            <td>
                                <p>{{$orderDetail->product->name}}</p>
                            </td>
                            <td> <img src="{{ asset('uploads/'.$orderDetail->product->image) }}" alt=""  width="100px" height="100px"> </td>
                            <td>{{$orderDetail->quantity}}</td>
                            <td>
                                ${{$orderDetail->product->price * $orderDetail->quantity }}
                            </td>

                        </tr>
                    @endforeach


                  </tbody>
                </table>
                <div style="display:flex;justify-content: center">
                    {{$orderDetails->links()}}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


@endsection
