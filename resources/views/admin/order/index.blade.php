@extends('admin.layouts.master')

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
                <div class="col-md-6">
                   <button class="btn btn-danger" id='order-deleteAllCheckbox' >Delete Selected</button>
                </div>
                <div class="col-md-6 ">
                    <a class="btn btn-success float-right" href="{{ route('order.create') }}">
                    Add
                    </a>
                </div>
        </div>
        <div>
            @if(session()->has('success'))
            <div class="alert alert-success" >
                {{ session()->get('success') }}
            </div>
            @endif
            @if(session()->has('failed'))
            <div class="alert alert-success" >
                {{ session()->get('failed') }}
            </div>
            @endif
        </div>
      <div class="row">
        <div class="col-md-12">
          <div class="card">

            <div class="card-body">
              <div class="table-responsive">
                <table class="table">
                  <thead class=" text-primary">
                    <tr>
                    <th>
                        <input type="checkbox" name="" id="order-selectAllCheckbox">
                    </th>
                    <th>
                      ID
                    </th>
                    <th>
                      Customer
                    </th>
                    <th>
                        Sub Total
                    </th>
                    <th>
                        Shipping
                    </th>
                    <th>
                        Discount
                    </th>
                    <th>
                      Payment Method
                    </th>
                    <th>
                        Status
                      </th>
                    <th>
                        Setting
                    </th>
                  </tr></thead>
                  <tbody>

                    @foreach ($orders as $key => $order)
                        <tr>
                            <td>
                                <input type="checkbox" name="order[]" id="" value="{{$order->id}}">
                            </td>
                            <td><p>{{$key+1}}</p></td>
                            <td><p><a href="http://">{{$order->user->email}}</a> </p></td>
                            <td>
                                <p>${{$order->subtotal}}</p>
                            </td>
                            <td>{{$order->shipping == 1?"Pick up in-store":($order->shipping == 2?"Delivery":"Free Ship") }}</td>
                            <td>${{$order->discount?$order->discount:0}}</td>
                            <td>
                                {{$order->payment_method == 1 ? "Cash On Delivery":"Credit Card" }}
                            </td>
                            <td>
                                <p class="badge badge-{{$order->status ==1?"danger":($order->status ==2 ?"primary":"success")}}">{{$order->status ==1?"New":($order->status ==2?"Paid":"Pending")}}</p>
                            </td>
                            <td style="width: 20%">
                                <a disabled  href="{{ route('order.orderdetail',$order->id) }}" class="btn btn-primary">
                                    <span class="material-icons">remove_red_eye</span>
                                </a>
                                <button id="order-{{$order->id}}" onclick="confirmDelete({{$order->id}},'order')" data-id="{{$order->id}}" data-name="{{$order->name}}" class="btn btn-danger">
                                    <span class="material-icons">delete</span>
                                    <form action="{{ route('order.destroy',$order->id)}}" hidden method="POST">
                                        @csrf
                                        @method('DELETE')
                                    </form>
                                </button>

                            </td>
                        </tr>
                    @endforeach


                  </tbody>
                </table>
                <div style="display:flex;justify-content: center">
                    {{$orders->links()}}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


@endsection
