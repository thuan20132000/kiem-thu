@extends('admin.layouts.master')

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
                <div class="col-md-6">
                   <button class="btn btn-danger" id='category-deleteAllCheckbox' >Delete Selected</button>
                </div>
                <div class="col-md-6 ">
                    <a class="btn btn-success float-right" href="{{ route('category.create') }}">
                    Add
                    </a>
                </div>
        </div>
        <div>
            @if(session()->has('success'))
            <div class="alert alert-success" >
                {{ session()->get('success') }}
            </div>
            @endif
            @if(session()->has('failed'))
            <div class="alert alert-success" >
                {{ session()->get('failed') }}
            </div>
            @endif
        </div>
      <div class="row">
        <div class="col-md-12">
          <div class="card">

            <div class="card-body">
              <div class="table-responsive">
                <table class="table">
                  <thead class=" text-primary">
                    <tr>
                    <th>
                        <input type="checkbox" name="" id="category-selectAllCheckbox">
                    </th>
                    <th>
                      ID
                    </th>
                    <th>
                      Name
                    </th>

                    <th>
                      Image
                    </th>
                    <th>
                        Setting
                    </th>
                  </tr></thead>
                  <tbody>

                    @foreach ($categories as $key => $category)
                        <tr>
                            <td>
                                <input type="checkbox" name="category[]" id="" value="{{$category->id}}">
                            </td>
                            <td><p>{{$key+1}}</p></td>
                            <td><p>{{$category->name}}</p></td>
                            <td>
                                <img src="{{ asset('uploads/'.$category->image) }}" width="100" alt="" srcset="">
                            </td>
                            <td style="width: 20%">
                                <a href="{{ route('category.edit',$category->id) }}" class="btn btn-primary">
                                        <span class="material-icons">construction</span>
                                </a>
                                <button id="category-{{$category->id}}" onclick="confirmDelete({{$category->id}},'category')" data-id="{{$category->id}}" data-name="{{$category->name}}" class="btn btn-danger">
                                    <span class="material-icons">delete</span>
                                    <form action="{{ route('category.destroy',$category->id)}}" hidden method="POST">
                                        @csrf
                                        @method('DELETE')
                                    </form>
                                </button>

                            </td>
                        </tr>
                    @endforeach


                  </tbody>
                </table>
                <div style="display:flex;justify-content: center">
                    {{$categories->links()}}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


@endsection
