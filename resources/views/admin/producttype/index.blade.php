@extends('admin.layouts.master')

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
                <div class="col-md-6">
                   <button class="btn btn-danger" id='producttype-deleteAllCheckbox' >Delete Selected</button>
                </div>
                <div class="col-md-6 ">
                    <a class="btn btn-success float-right" href="{{ route('producttype.create') }}">
                    Add
                    </a>
                </div>
        </div>
        <div>
            @if(session()->has('success'))
            <div class="alert alert-success" >
                {{ session()->get('success') }}
            </div>
            @endif
            @if(session()->has('failed'))
            <div class="alert alert-success" >
                {{ session()->get('failed') }}
            </div>
            @endif
        </div>
      <div class="row">
        <div class="col-md-12">
          <div class="card">

            <div class="card-body">
              <div class="table-responsive">
                <table class="table">
                  <thead class=" text-primary">
                    <tr>
                    <th>
                        <input type="checkbox" name="" id="producttype-selectAllCheckbox">
                    </th>
                    <th>
                      ID
                    </th>
                    <th>
                      Name
                    </th>
                    <th>
                      Category
                    </th>
                    <th>
                      Image
                    </th>
                    <th>
                        Setting
                    </th>
                  </tr></thead>
                  <tbody>

                    @foreach ($producttypes as $key => $producttype)
                        <tr>
                            <td>
                                <input type="checkbox" name="producttype[]" id="" value="{{$producttype->id}}">
                            </td>
                            <td><p>{{$key+1}}</p></td>
                            <td><p>{{$producttype->name}}</p></td>
                            <td> <p class="badge badge-info">{{$producttype->category->name}}</p> </td>
                            <td>
                                <img src="{{ asset('uploads/'.$producttype->image) }}" width="100" alt="" srcset="">
                            </td>
                            <td style="width: 20%">
                                <a href="producttype/{{$producttype->id}}/edit?id={{$producttype->id}}" class="btn btn-primary">
                                        <span class="material-icons">construction</span>
                                </a>
                                <button id="producttype-{{$producttype->id}}" onclick=confirmDelete({{$producttype->id}},'producttype') data-id="{{$producttype->id}}" data-name="{{$producttype->name}}" class="btn btn-danger">
                                    <span class="material-icons">delete</span>
                                    <form action="{{ route('producttype.destroy',['producttype'=>$producttype])}}" hidden method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <input name="id" type="text"  value="{{$producttype->id}}">
                                    </form>
                                </button>

                            </td>
                        </tr>
                    @endforeach


                  </tbody>
                </table>
                <div style="display:flex;justify-content: center">
                    {{$producttypes->links()}}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


@endsection
