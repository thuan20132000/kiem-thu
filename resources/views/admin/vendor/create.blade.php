@extends('admin.layouts.master')
<link rel="stylesheet" href="{{ asset('assets/adminForm.css') }}">

@section('content')
<div class="content">
    <div class="kt--bg-header kt--pd-20">
            <h3>Vendor</h3>
    </div>
    <div class="kt--pd-100">
        <div>
            @if(session()->has('success'))
            <div class="alert alert-success" >
                {{ session()->get('success') }}
            </div>
            @endif
            @if(session()->has('failed'))
            <div class="alert alert-success" >
                {{ session()->get('failed') }}
            </div>
            @endif
        </div>
        <form method="POST" action="{{ route('category.store')}}" enctype="multipart/form-data">
            @csrf
            <div class="form-row kt--mgb-20">
              <div class="form-group col-md-6 is-focused">
                <label for="inputName">Name</label>
                <input name="name" value="{{old('name')}}" type="text" class="form-control form-control @error('name') is-invalid @enderror" id="inputName">
                @error('name')
                    <span class="error invalid-feedback">{{$message}}</span>
                @enderror
              </div>
              <div class="form-group col-md-6 is-focused">
                <label for="inputPhone">Phone</label>
                <input name="phone" value="{{old('phone')}}" type="text" class="form-control @error('phone') is-invalid @enderror" id="inputPhone">
                @error('phone')
                    <span class="error invalid-feedback">{{$message}}</span>
                @enderror
              </div>
            </div>

            <div class="form-group is-focused kt--mgb-20">
                <label for="inputAddress">Address</label>
                <input name="address"  value="{{old('address')}}"  type="text" class="form-control @error('address') is-invalid @enderror" id="inputAddress" placeholder="1234 Main St">
                @error('address')
                    <span class="error invalid-feedback">{{$message}}</span>
                @enderror
            </div>
            <div class="custom-file kt--mgt-20" id="uploadForm">
                <input type="file" name="image"  id="file">
            </div>
            <div class="kt--mgt-20">
                <button type="submit" class="btn btn-primary float-right">Create</button>
            </div>

          </form>
    </div>

</div>
@endsection
