@extends('admin.layouts.master')

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
                <div class="col-md-6">
                   <button class="btn btn-danger" id='vendor-deleteAllCheckbox' >Delete Selected</button>
                </div>
                <div class="col-md-6 ">
                    <a class="btn btn-success float-right" href="{{ route('vendor.create') }}">
                    Add
                    </a>
                </div>
        </div>
        <div>
            @if(session()->has('success'))
            <div class="alert alert-success" >
                {{ session()->get('success') }}
            </div>
            @endif
            @if(session()->has('failed'))
            <div class="alert alert-warning" >
                {{ session()->get('failed') }}
            </div>
            @endif
        </div>
      <div class="row">
        <div class="col-md-12">
          <div class="card">

            <div class="card-body">
              <div class="table-responsive">
                <table class="table">
                  <thead class=" text-primary">
                    <tr>
                    <th>
                        <input type="checkbox" name="" id="vendor-selectAllCheckbox">
                    </th>
                    <th>
                      ID
                    </th>
                    <th>
                      Name
                    </th>
                    <th>
                      Phone
                    </th>
                    <th>
                      Address
                    </th>
                    <th>
                      Image
                    </th>
                    <th>
                        Setting
                    </th>
                  </tr></thead>
                  <tbody>

                    @foreach ($vendors as $key => $vendor)
                        <tr>
                            <td>
                                <input type="checkbox" name="vendor[]" id="" value="{{$vendor->id}}">
                            </td>
                            <td><p>{{$key+1}}</p></td>
                            <td><p>{{$vendor->name}}</p></td>
                            <td><p>{{$vendor->phone}}</p></td>
                            <td><p>{{$vendor->address}}</p></td>
                            <td>
                                <img src="{{ asset('uploads/'.$vendor->image) }}" width="100" alt="" srcset="">
                            </td>
                            <td style="width: 20%">
                                <a href="{{ route('vendor.edit',$vendor->id) }}" class="btn btn-primary">
                                        <span class="material-icons">construction</span>
                                </a>
                                <button id="vendor-{{$vendor->id}}" onclick=confirmDelete({{$vendor->id}},'vendor') data-id="{{$vendor->id}}" data-name="{{$vendor->name}}" class="btn btn-danger">
                                    <span class="material-icons">delete</span>
                                    <form action="{{ route('vendor.destroy',$vendor->id)}}" hidden method="POST">
                                        @csrf
                                        @method('DELETE')
                                    </form>
                                </button>

                            </td>
                        </tr>
                    @endforeach


                  </tbody>
                </table>
                <div style="display:flex;justify-content: center">
                    {{$vendors->links()}}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


@endsection
