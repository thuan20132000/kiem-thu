@extends('pages.layouts.master')

@section('content')

<section class="shop spad">
    <div class="container">
        <div>
            @if(session()->has('success'))
            <div class="alert alert-success" >
                {{ session()->get('success') }}
            </div>
            @endif
            @if(session()->has('failed'))
            <div class="alert alert-success" >
                {{ session()->get('failed') }}
            </div>
            @endif
        </div>
        <div class="row">
            <form method="POST" action="{{route('page.handleRegister')}}" >
                @csrf
                <div class="form-row">

                  <div class="form-group col-md-12">
                    <label for="inputEmail4">Email</label>
                    <input name="email" type="email" class="form-control" id="inputEmail4">
                  </div>

                  <div class="form-group col-md-12">
                    <label for="inputPassword4">Password</label>
                    <input name="password" type="password" class="form-control" id="inputPassword4">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputName">Name</label>
                  <input name="name" type="text" class="form-control" id="inputName" placeholder="Enter Name">
                </div>
                <button type="submit" class="btn btn-primary">Sign in</button>
              </form>
        </div>
    </div>
</section>

@endsection
