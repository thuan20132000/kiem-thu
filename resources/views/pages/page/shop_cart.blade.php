@extends('pages.layouts.master');

@section('content')
    <!-- Shop Cart Section Begin -->
    <section class="shop-cart spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="shop__cart__table">
                        <table>
                            <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Total</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody id="cart_item_list">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="cart__btn">
                        <a href="#">Continue Shopping</a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="cart__btn update__btn">
                        <a href="#"><span class="icon_loading"></span> Update cart</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="discount__content">
                        <h6>Discount codes</h6>
                        <form action="#">
                            <input type="text" placeholder="Enter your coupon code">
                            <button type="submit" class="site-btn">Apply</button>
                        </form>
                    </div>
                </div>
                <div class="col-lg-4 offset-lg-2">
                    <div class="cart__total__procced">
                        <h6>Cart total</h6>
                        <ul>
                            <li>Subtotal <span class="cart__subtotal"> 750.0</span><span>$</span></li>
                        </ul>
                        <a href="#" class="primary-btn btn-checkout" >Proceed to checkout</a>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!-- Shop Cart Section End -->
    <script src="{{ asset('page/js/jquery-3.3.1.min.js') }} "></script>
    <script>



            var productCart = [];
            var product_ids = [];


            function cartItem(id,name,image,price){

                return   `<tr class="product-item product-${id}" data-id=${id}>
                            <td class="cart__product__item">
                                <img src="{{asset('uploads/${image}')}}" alt="" width="100" height="100">
                                <div class="cart__product__item__title">
                                    <h6>${name}</h6>
                                    <div class="rating">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </div>
                                </div>
                            </td>
                            <td class="cart__price"><span>$</span><span class="price">${price}</span></td>
                            <td class="cart__quantity">
                                <div class="pro-qty">
                                    <input type="number" value="1"  id="quantity" name="quantity" min="1" max="100">
                                </div>
                            </td>
                            <td class="cart__total"><span>$</span><span class="price">${price}</span> </td>
                            <td class="cart__close"><span class="icon_close"></span></td>
                        </tr>`;
            }

            function setNumberProductCart($number){
                        $('.cart-count').text($number);
            }
            const fetchProductCart =  () => {


                    var cartItems = window.localStorage.getItem('item1');
                    product_ids = JSON.parse(cartItems);
                    console.log('cart Items',JSON.parse(cartItems));

                    setNumberProductCart(product_ids.length);



                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    var request = $.ajax({
                        url: '/ajaxcartproducts',
                        method: "GET",
                        data:{id:JSON.parse(cartItems)}
                    });

                    request.done(function( data ) {
                        // console.log(data.data);
                        data.data.map(e => productCart.push(e));
                        // data.data.map(e => $('#cart_item_list').append(cartItem(1,e.name,e.image,e.price)));
                        productCart.map(e => $('#cart_item_list').append(cartItem(e.id,e.name,e.image,e.price)) );
                        handleSubTotal();
                    });

                    request.fail(function( jqXHR, textStatus ) {
                        console.log( "Request failed: " + textStatus );
                    });
            }
            fetchProductCart();

            function handleSubTotal(){

            let subtotal = 0;
            $('.product-item').each(function(index){
                let idProduct = $(this).data('id');
                let totalPrice = $(this).find('.cart__total .price').text();
                let quantityItem = $(this).find('.cart__quantity .pro-qty input').val();

                subtotal += parseFloat(totalPrice);

            });

            $('.cart__subtotal').text(parseFloat(subtotal).toFixed(2));

            }
            handleSubTotal();





        $('document').ready(function(){




            $('.icon_close').on('click',function(){
                let id = $(this).parent().parent().data('id');

                productCart.forEach((element,index) => {
                    if(element.id == id){
                        product_ids.splice(index,1);

                    }

                });

                $(this).parent().parent().remove();
                handleSubTotal();

            });

            $('.cart__quantity .pro-qty input').on('change',function(){
                let currentItem = $(this).parent().parent().parent();
                let itemPrice = currentItem.find('.cart__price .price').text();
                let quantityItem = currentItem.find('.cart__quantity .pro-qty input').val();
                let totalPrice =  parseFloat(quantityItem) * parseFloat(itemPrice);
                currentItem.find('.cart__total .price').text(totalPrice.toFixed(2));

                handleSubTotal();

            });

            var OrderProductList = [];
            function OrderItem(id,quantity,price,subtotal){
                this.id = id;
                this.quantity = quantity;
                this.price =price;
                this.subtotal = subtotal;
            }



            var statusOrder = false;
            function handleCheckOut(){
                    let subtotal = 0;
                    $('.product-item').each(function(index){
                        let idProduct = $(this).data('id');
                        let itemPrice = $(this).find('.cart__price .price').text();
                        let totalPrice = $(this).find('.cart__total .price').text();
                        let quantityItem = $(this).find('.cart__quantity .pro-qty input').val();

                        subtotal += parseFloat(totalPrice);
                        let item = new OrderItem(idProduct,parseInt(quantityItem),itemPrice,totalPrice);
                        OrderProductList.push(item);
                    });

                    $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                    });

                    var request = $.ajax({
                        url: '/ajaxorderproducts',
                        method: 'POST',
                        data:{
                            orders:OrderProductList,
                            subtotal:subtotal
                        }
                    });

                    request.done(function( data ) {
                        console.log(data);
                        if(data.error === false){
                            productCart = [];
                            product_ids = [];
                            OrderProductList = [];

                            setTimeout(() => {
                                location.reload();
                            }, 1000);
                        }
                    });

                    request.fail(function( jqXHR, textStatus ) {
                        console.log( "Request failed: " + textStatus );
                        statusOrder = false;

                    });

            }



            $('.btn-checkout').on('click',function(evt){
                evt.preventDefault();
                handleCheckOut();

                return ;

            })







        });



        window.onbeforeunload = function(event){
            localStorage.setItem('item1',JSON.stringify(product_ids));
        }




    </script>


@endsection
