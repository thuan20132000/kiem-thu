
@extends('pages.layouts.master')

@section('content')
    @include('pages.layouts.categorySection')
    @include('pages.layouts.productSection')

    <script src="{{ asset('page/js/jquery-3.3.1.min.js') }} "></script>

    <script src="{{ asset('assets/addtocart.js') }}">

    </script>

@endsection
