

@include('pages.layouts.header')
@include('pages.layouts.nav')

@yield('content')

@include('pages.layouts.footer')
