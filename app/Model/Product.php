<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //

    public function producttype()
    {
        return $this->belongsTo(ProductType::class);
    }

    public function vendor()
    {
        return $this->belongsTo(Vendor::class);
    }

    public function orderDetail()
    {
        return $this->hasMany('App\Model\OrderDetail','product_id','id');
    }
}
