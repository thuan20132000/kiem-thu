<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    public function productTypes()
    {
        return $this->hasMany(ProductType::class);
    }

    public function products()
    {
        return $this->hasManyThrough(Product::class,ProductType::class,'category_id','producttype_id','id');
    }
}
