<?php

namespace App\Http\Controllers;

use App\Model\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categories = Category::orderBy('updated_at','desc')->paginate(10);
        return view('admin.category.index',['categories'=>$categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name'=>'required'
            ],[
                'name.required'=>'Please enter category name'
            ]
        );

        $fileName = 'default.jpg';
        if ($request->image) {
            try {
                $fileName = 'category-'.time().'.'.$request->image->extension();
                $request->image->move(public_path('uploads'), $fileName);
            } catch (\Throwable $th) {
                throw $th;
                $errors = true;
            }

        }

        try {
            $category = new Category();
            $category->name = $request->name;
            $category->image = $fileName;
            $category->save();
        } catch (\Throwable $e) {
            return redirect()->back()->with('failed','Create Failed');
        }


        return redirect()->back()->with('success','Create Successfully');





    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
        return view('admin.category.edit',['category'=>$category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        //
        $request->validate([
            'name'=>'required'
            ],[
                'name.required'=>'Please enter category name'
            ]
        );

        $fileName = 'default.jpg';
        if ($request->image) {
            try {
                $fileName = 'category-'.time().'.'.$request->image->extension();
                $request->image->move(public_path('uploads'), $fileName);
            } catch (\Throwable $th) {
                throw $th;
                $errors = true;
            }

        }

        try {

            $category->name = $request->name;
            if($request->image){
                $category->image = $fileName;
            }
            $category->save();
        } catch (\Throwable $e) {
            return redirect()->back()->with('failed','Create Failed');
        }


        return redirect()->back()->with('success','Create Successfully');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
        $errors = null;
        try {
            $category->delete();
        } catch (\Throwable $th) {
            return redirect()->back()->with('failed','Delete  failed!!!');
        }

        return redirect()->back()->with('success','Delete  success!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function destroyMass(Request $request)
    {
        $errors = null;
        try {
            //...
            Category::destroy($request->ids);
        } catch (\Throwable $th) {
            $errors = "failed";
        }

        return response()->json([
            'state'=>$errors
        ]);
    }
}
