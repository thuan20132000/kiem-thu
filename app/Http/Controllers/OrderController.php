<?php

namespace App\Http\Controllers;

use App\Model\Order;
use App\Model\OrderDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $orders = Order::orderBy('created_at','desc')->paginate(10);
        return view('admin.order.index',['orders'=>$orders]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show($order_id)
    {
        //
        $orderDetails = OrderDetail::where('order_id',$order_id)->paginate(10);
        return view('admin.order.orderDetail',['orderDetails'=>$orderDetails]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
        OrderDetail::where('order_id',$order->id)->delete();
        $order->delete();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function destroyMass(Request $request)
    {
        $errors = null;
        try {
            //...
        } catch (\Throwable $th) {
            $errors = "failed";
        }

        return response()->json([
            'state'=>$errors
        ]);
    }


    public function handleOrderAjax(Request $request){

        $orders = $request->orders;
        $user = Auth::user();

        $error = false;
        try {
            //code...
            $newOrder = new Order();
            $newOrder->subtotal = $request->subtotal;
            $newOrder->user_id = $user->id;
            $newOrder->save();


            try {
                //code...
               foreach ($orders as $key => $order) {
                    $newOrderDetail = new OrderDetail();
                    $newOrderDetail->quantity = $order['quantity'];
                    $newOrderDetail->product_id = $order['id'];
                    $newOrderDetail->order_id = $newOrder->id;
                    $newOrderDetail->save();
               }

            } catch (\Throwable $th) {
                //throw $th;
                $error = $th;
            }

        } catch (\Throwable $th) {
            //throw $th;
            $error = $th;

        }


        // DB::table('order_detail')->insert([
        //     ['email' => 'taylor@example.com', 'votes' => 0],
        //     ['email' => 'dayle@example.com', 'votes' => 0],
        // ]);


        return response()->json([
            'error'=>$error
        ]);

    }



}
