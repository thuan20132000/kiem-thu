<?php

namespace App\Http\Controllers;

use App\Model\Category;
use App\Model\ProductType;
use Illuminate\Http\Request;

class ProductTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $producttypes = ProductType::orderBy('created_at','desc')->paginate(10);
        return view('admin.producttype.index',['producttypes'=>$producttypes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = Category::all();
        return view('admin.producttype.create',['categories'=>$categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name'=>'required',
            'category_id'=>'required'
            ],[
                'name.required'=>'Please enter product type name',
                'category_id.required'=>"Please choose category"
            ]
        );

        $fileName = 'default.jpg';
        if ($request->image) {
            try {
                $fileName = 'producttype-'.time().'.'.$request->image->extension();
                $request->image->move(public_path('uploads'), $fileName);
            } catch (\Throwable $th) {
                throw $th;
                $errors = true;
            }

        }

        try {
            $productType = new ProductType();
            $productType->name = $request->name;
            $productType->image = $fileName;
            $productType->category_id = $request->category_id;
            $productType->save();
        } catch (\Throwable $e) {
            return redirect()->back()->with('failed','Create Failed');
        }


        return redirect()->back()->with('success','Create Successfully');


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\ProductType  $productType
     * @return \Illuminate\Http\Response
     */
    public function show(ProductType $productType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\ProductType  $productType
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductType $productType,Request $request)
    {
        //
        $categories = Category::all();
        $productType = ProductType::find($request->id);
        return view('admin.producttype.edit',['categories'=>$categories,'productType'=>$productType]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\ProductType  $productType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $request->validate([
            'name'=>'required',
            'category_id'=>'required'
            ],[
                'name.required'=>'Please enter product type name',
                'category_id.required'=>"Please choose category"
            ]
        );

        $fileName = 'default.jpg';
        if ($request->image) {
            try {
                $fileName = 'producttype-'.time().'.'.$request->image->extension();
                $request->image->move(public_path('uploads'), $fileName);
            } catch (\Throwable $th) {
                throw $th;
                $errors = true;
            }

        }
        try {
            $productType = ProductType::where('id',$request->id)->first();

            $productType->name = $request->name;
            if($request->image){
                $productType->image = $fileName;
            }
            $productType->category_id = $request->category_id;
            $productType->save();
        } catch (\Throwable $e) {
            return redirect()->back()->with('failed','Update Failed');
        }


        return redirect()->back()->with('success','Update Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\ProductType  $productType
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        try {
            ProductType::where('id',$request->id)->delete();
        } catch (\Throwable $th) {
            return redirect()->back()->with('failed','Delete  failed!!!');
        }

        return redirect()->back()->with('success','Delete  success!');


    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function destroyMass(Request $request)
    {
        $errors = null;
        try {
            //...
            ProductType::destroy($request->ids);
        } catch (\Throwable $th) {
            $errors = "failed";
        }

        return response()->json([
            'state'=>$errors
        ]);
    }
}
