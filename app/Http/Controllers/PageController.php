<?php

namespace App\Http\Controllers;

use App\Model\Category;
use App\Model\Product;
use App\Model\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class PageController extends Controller
{
    //

    public function index(){
        $categories = Category::all()->random(4);
        $products = Product::orderBy('created_at','desc')->take(12)->get();
        return view('pages.index',['categories'=>$categories,'products'=>$products]);
    }

    public function shop(Request $request){
        $categories = Category::all();

        $producttype = $request->producttype;
        if($producttype){
           $products = Product::where('producttype_id',$producttype)->orderBy('created_at','desc')->paginate(12);
        }else{
            $products = Product::orderBy('created_at','desc')->paginate(22);
        }

        return view('pages.page.shop',['categories'=>$categories,'products'=>$products]);
    }

    public function cart(Request $request){
        return view('pages.page.shop_cart');
    }

    public function login(Request $request){
        return view('pages.page.login');
    }

    public function handleLogin(Request $request){

        $check = Auth::attempt(['email' => $request->email, 'password' => $request->password]);
        if($check){
            return redirect()->route('page.home');
        }

        return redirect()->back()->with("failed","Login Failed");

    }



    public function register(Request $request){
        return view('pages.page.register');
    }

    public function handleRegister(Request $request){

        $request->validate([
            'email'=>'required',
            'name'=>'required',
            'password'=>'required'
        ]);
        $userRole = Role::where('name','user')->first();

        try {
            //code...
            $user = new User();
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->name = $request->name;
            $user->save();
            $user->roles()->attach($userRole);

        } catch (\Throwable $th) {
            //throw $th;
            return redirect()->back()->with('failed','Register Failed');
        }

        return redirect()->back()->with("success","Register Success");

    }

    public function logout(){
        Auth::logout();
        return redirect()->route('page.login');
    }


    public function getProductCart(Request $request){

        $products = Product::whereIn('id',$request->id)->get();
        return response()->json([
            'data'=>$products
        ]);
    }




}
