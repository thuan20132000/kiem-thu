<?php

namespace App\Http\Controllers;

use App\Model\Category;
use App\Model\Product;
use App\Model\ProductType;
use App\Model\Vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $products = Product::orderBy('created_at','desc')->paginate(10);
        return view('admin.product.index',['products'=>$products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $vendors = Vendor::all();
        $categories = Category::all();
        $producttypes = ProductType::all();
        return view('admin.product.create',['vendors'=>$vendors,'categories'=>$categories,'producttypes'=>$producttypes]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate(
            [
                'name'=>'required',
                'stock'=>'required|numeric',
                'price'=>'required|numeric',
                'vendor_id'=>'required',
                'producttype_id'=>'required',
                'category_id'=>'required'

            ],[
                'name.required'=>'Please enter  name',
                'stock.required'=>'Please enter stock',
                'stock.numeric'=>'Stock should be number',
                'price.required'=>"Please enter price",
                'price.numeric'=>'Price should be number',

            ]
        );

        $fileName = 'default.jpg';
        if ($request->image) {
            try {
                $fileName = 'prodyct-'.time().'.'.$request->image->extension();
                $request->image->move(public_path('uploads'), $fileName);
            } catch (\Throwable $th) {
                throw $th;
                $errors = true;
            }

        }
        try {
            $product = new Product();
            $product->name = $request->name;
            $product->image = $fileName;
            $product->slug = Str::slug($request->name,'-');
            $product->stock = $request->stock;
            $product->price = $request->price;
            $product->description = $request->description;
            $product->producttype_id = $request->category_id;
            $product->vendor_id = $request->vendor_id;
            $product->save();
        } catch (\Throwable $e) {
            return redirect()->back()->with('failed','Create Failed');
        }


        return redirect()->back()->with('success','Create Successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
        $vendors = Vendor::all();
        $producttypes = ProductType::all();
        $categories = Category::all();

        return view('admin.product.edit',['product'=>$product,'vendors'=>$vendors,'categories'=>$categories,'producttypes'=>$producttypes]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
        $request->validate(
            [
                'name'=>'required',
                'stock'=>'required|numeric',
                'price'=>'required|numeric',
                'vendor_id'=>'required',
                'producttype_id'=>'required',
                'category_id'=>'required'

            ],[
                'name.required'=>'Please enter  name',
                'stock.required'=>'Please enter stock',
                'stock.numeric'=>'Stock should be number',
                'price.required'=>"Please enter price",
                'price.numeric'=>'Price should be number',

            ]
        );

        $fileName = 'default.jpg';
        if ($request->image) {
            try {
                $fileName = 'product-'.time().'.'.$request->image->extension();
                $request->image->move(public_path('uploads'), $fileName);
            } catch (\Throwable $th) {
                throw $th;

            }

        }

        try {

            $product->name = $request->name;
            if($request->image){
                $product->image = $fileName;
            }
            $product->slug = Str::slug($request->name,'-');
            $product->stock = $request->stock;
            $product->price = $request->price;
            $product->description = $request->description;
            $product->producttype_id = $request->category_id;
            $product->vendor_id = $request->vendor_id;
            $product->update();
        } catch (\Throwable $e) {
            return redirect()->back()->with('failed','Update Failed');
        }

        // dd($product);
        return redirect()->back()->with('success','Update Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //

        try {
            $product->delete();
        } catch (\Throwable $th) {
            return redirect()->back()->with('failed','Delete  failed!!!');
        }

        return redirect()->back()->with('success','Delete  success!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function destroyMass(Request $request)
    {
        $errors = null;
        try {
            //...
            Product::destroy($request->ids);
        } catch (\Throwable $th) {
            $errors = "failed";
        }

        return response()->json([
            'state'=>$errors
        ]);
    }



    /**
     * Get producttype by category ajax
     *
     * @param  \App\Model\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function getProductTypeByCategory(Request $request)
    {

        try {
            //code...
            $category_id =  $request->id;
            $producttypes = ProductType::where('category_id',$category_id)->get();

        } catch (\Throwable $th) {
            //throw $th;
            return response()->json('error',404);

        }

        return response()->json([
            'data'=>$producttypes
        ]);

    }
}
