<?php

namespace App\Http\Controllers;

use App\Model\Vendor;
use Illuminate\Http\Request;

class VendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $vendors = Vendor::orderBy('updated_at')->paginate(10);
        return view('admin.vendor.index',['vendors'=>$vendors]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.vendor.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd($request->all());
        $errors = null;
        $request->validate([
            'name' => 'required',
            'phone' => 'required',
            'address'=>'required',
        ],[
            'name.required'=>'Please enter  name!!!',
            'phone.required'=>'Please enter  phone!!!',
            'address.required'=>'Please enter address!!!',
        ]);

        $fileName = 'default.jpg';
        if ($request->image) {
            try {
                $fileName = 'vendor-'.time().'.'.$request->image->extension();
                $request->image->move(public_path('uploads'), $fileName);
            } catch (\Throwable $th) {
                throw $th;
                $errors = true;
            }

        }

        try {
            $vendor = new Vendor();
            $vendor->name = $request->name;
            $vendor->phone = $request->phone;
            $vendor->address = $request->address;
            $vendor->image = $fileName;
            $vendor->save();
        } catch (\Throwable $e) {
            throw $e;
            $errors = true;
        }


        if($errors === true){
            return redirect()->back()->with('failed','Create vendor failed!!!');
        }
        return redirect()->back()->with('success','Created vendor successfull!');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function show(Vendor $vendor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function edit(Vendor $vendor)
    {
        //
        // dd($vendor);
        return view('admin.vendor.edit',['vendor'=>$vendor]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vendor $vendor)
    {
        //
        // dd($request->all());
        $errors = null;
        $request->validate([
            'name' => 'required',
            'phone' => 'required|numeric',
            'address'=>'required',
        ],[

            'name.required'=>"Name shouldn't be empty !!!",
            'phone.required'=>"Phone shouldn't be empty !!!",
            'address.required'=>"Address shouldn't be empty !!!",
        ]);

        $fileName = '';
        if ($request->image) {
            try {
                $fileName = 'vendor-'.time().'.'.$request->image->extension();
                $request->image->move(public_path('uploads'), $fileName);
            } catch (\Throwable $th) {
                throw $th;
                $errors = true;
            }
        }


        try {
            $vendor->name = $request->name;
            $vendor->phone = $request->phone;
            $vendor->address = $request->address;
            if($request->image){
                $vendor->image = $fileName;
            }
            $vendor->update();
        } catch (\Throwable $e) {
            throw $e;
            $errors = true;
        }

        if($errors === true){
            return redirect()->back()->with('failed','Updated vendor failed!!!');
        }

        return redirect()->back()->with('success','Updated vendor successfull!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vendor $vendor)
    {
        //

        try {
            $vendor->delete();
        } catch (\Throwable $th) {
            return redirect()->back()->with('failed','Delete vendor failed. Please Delete children elements before remove it.');
        }

        return redirect()->back()->with('success','Delete vendor success!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function destroyMass(Request $request)
    {
        $errors = null;
        try {
            //...
            Vendor::destroy($request->ids);

        } catch (\Throwable $th) {
            $errors = "failed";
        }
        return response()->json([
            'state'=>$errors
        ]);
    }
}
