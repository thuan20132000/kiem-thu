<?php

use App\Model\Category;
use App\Model\Order;
use App\Model\Product;
use App\Model\ProductType;
use App\Model\Role;
use Illuminate\Database\Seeder;

use App\Model\Vendor;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);
        factory(Vendor::class, 10)->create();
        // factory(Category::class,10)->create();
        // factory(ProductType::class,20)->create();
        // factory(Product::class,100)->create();
        // factory(User::class,10)->create();
        // factory(Order::class,10)->create();


    }
}
