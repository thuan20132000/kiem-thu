<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Order;
use App\User;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
    return [
        //
        'subtotal'=>$faker->numberBetween(20,1000),
        'shipping'=>$faker->numberBetween(1,3),
        'discount'=>$faker->numberBetween(2,8),
        'payment_method'=>$faker->numberBetween(1,3),
        'status'=>$faker->numberBetween(1,3),
        'user_id'=>function(){
            return User::all()->random();
        }
    ];
});
