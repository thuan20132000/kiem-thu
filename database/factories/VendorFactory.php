<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Vendor;
use Faker\Generator as Faker;

$factory->define(Vendor::class, function (Faker $faker) {
    return [
        //
        'name' => $faker->name,
        'image' => $faker->imageUrl(),
        'address' => $faker->address,
        'phone' => $faker->phoneNumber
    ];
});
