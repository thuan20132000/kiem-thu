<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Category;
use App\Model\ProductType;
use Faker\Generator as Faker;

$factory->define(ProductType::class, function (Faker $faker) {
    return [
        //
        'name'=>$faker->word,
        'image'=>$faker->imageUrl,
        'category_id'=>function(){
            return Category::all()->random();
        }
    ];
});
