<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Model\Product;
use App\Model\ProductType;
use App\Model\Vendor;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        //
        'name'=>$faker->word,
        'slug'=>$faker->slug,
        'description'=>$faker->paragraph,
        'image'=>$faker->imageUrl,
        'stock'=>$faker->numberBetween(10,100),
        'price'=>$faker->numberBetween(10,1000),
        'vendor_id'=>function(){
            return Vendor::all()->random();
        },
        'producttype_id'=>function(){
            return ProductType::all()->random();
        }
    ];
});
