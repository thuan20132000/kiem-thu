
    /**
     * created by Thuan
     * created at 15/07/2020
     * description: function to preview image in create and edit
     */
    function filePreview(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#uploadForm + img').remove();
                $('#uploadForm').after('<img src="' + e.target.result + '" width="220" height="220"/>');
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#file").change(function() {
        filePreview(this);
    });


    /**
     * created by Thuan
     * created at 15/07/2020
     * description: function to delete an item in list
     */
    function confirmDelete(id,name){
        let item = $(`#${name}-${id}`).data('name');
        let check = confirm(`Do you want to remove << ${item} >> ?`);
        if(check){
            $(`#${name}-${id} form`).submit();
        }
    }


    /**
     * created by Thuan
     * created at 15/07/2020
     * description: function to select All item in list need to delete
     */
    function selectAllCheckbox(name){

        $(`#${name}-selectAllCheckbox`).click(function(){
            $(`input:checkbox[name="${name}[]"]`).prop('checked',this.checked);
        });

    }
    selectAllCheckbox('vendor');
    selectAllCheckbox('category');
    selectAllCheckbox('producttype');
    selectAllCheckbox('product');





    /**
     * created by Thuan
     * created at 15/07/2020
     * description: function to select All item in list need to delete
     */
    function deleteAllCheckbox(name){

        $(`#${name}-deleteAllCheckbox`).click(function(){
            var destroyItems = [];
            $.each($(`input[name='${name}[]']:checked`), function() {
                destroyItems.push($(this).val());
            });

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var request = $.ajax({
                url: `${name}/destroyMass`,
                method: "POST",
                data: { ids : destroyItems },
            });

            request.done(function( msg ) {
                console.log('data '+msg.state);
                location.reload();
            });

            request.fail(function( jqXHR, textStatus ) {
                alert( "Request failed: " + textStatus );
            });

        });
    }
    deleteAllCheckbox('vendor');
    deleteAllCheckbox('category');
    deleteAllCheckbox('producttype');
    deleteAllCheckbox('product');





$('document').ready(function(){
    /**
     * created by Thuan
     * created at 15/07/2020
     * description: function to get producttype by category
     */
    function getProductTypeByCategory(id){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var request = $.ajax({
            url: '/admin/ajaxproducttypes',
            method: "GET",
            data:{id:id}
        });

        request.done(function( data ) {
            // console.log(data.data);
            let opt = `<option selected >Choose product types...</option>`;
            data.data.map(e => opt = opt + `<option value='${e.id}' >${e.name}</option>` )
            $('#producttype').html(opt);
        });

        request.fail(function( jqXHR, textStatus ) {
            alert( "Request failed: " + textStatus );
        });
    }

    $('#category').on('change',function(){
        let category_id = this.value;
        getProductTypeByCategory(category_id);

    })


})


