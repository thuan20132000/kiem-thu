<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();




Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function() {

    Route::get('/home', 'HomeController@index')->name('admin.home');


    /**
     * created by thuan
     * created at 12/07/2020
     * Description: Display Dashboard Home
     */
    // Route::get('/','HomeController@home');

    /**
     * created by thuan
     * created at 12/07/2020
     * Description: CRUD for VendorController
     */
    Route::resource('/vendor', 'VendorController');
    Route::post('/vendor/destroyMass','VendorController@destroyMass')->name('vendor.destroyMass');

    /**
     * created by thuan
     * created at 12/07/2020
     * Description: CRUD for VendorController
     */
    Route::resource('/category', 'CategoryController');
    Route::post('/category/destroyMass','CategoryController@destroyMass')->name('category.destroyMass');

    /**
     * created by thuan
     * created at 12/07/2020
     * Description: CRUD for VendorController
     */
    Route::resource('/producttype', 'ProductTypeController');
    Route::post('/producttype/destroyMass','ProductTypeController@destroyMass')->name('producttype.destroyMass');


    /**
     * created by thuan
     * created at 12/07/2020
     * Description: CRUD for VendorController
     */
    Route::resource('/product', 'ProductController');
    Route::post('/product/destroyMass','ProductController@destroyMass')->name('product.destroyMass');
    Route::get('/ajaxproducttypes','ProductController@getProductTypeByCategory')->name('product.getProductType');


    /**
     * created by thuan
     * created at 12/07/2020
     * Description: CRUD for VendorController
     */
    Route::resource('/order', 'OrderController');
    Route::get('/orderdetail/{order_id}','OrderController@show')->name('order.orderdetail');
    Route::post('/order/destroyMass','OrderController@destroyMass')->name('order.destroyMass');
    // Route::get('/ajaxproducttypes','ProductController@getProductTypeByCategory')->name('product.getProductType');

     /**
     * created by thuan
     * created at 12/07/2020
     * Description: CRUD for VendorController
     */
    // Route::resource('/orderdetail', 'OrderDetailController');
    // Route::post('/orderdetail/destroyMass','OrderController@destroyMass')->name('order.destroyMass');
    // Route::get('/ajaxproducttypes','ProductController@getProductTypeByCategory')->name('product.getProductType');



});

Route::get('/login','PageController@login')->name('page.login');
Route::post('/login','PageController@handleLogin')->name('page.handelLogin');
Route::get('/register','PageController@register')->name('page.register');
Route::post('/register','PageController@handleRegister')->name('page.handleRegister');
Route::get('/logout','PageController@logout')->name('page.logout');

Route::get('/','PageController@index')->name('page.home');
Route::get('/shop','PageController@shop')->name('page.shop');
Route::get('/cart','PageController@cart')->name('page.cart');
Route::get('/ajaxcartproducts','PageController@getProductCart');

Route::post('/ajaxorderproducts','OrderController@handleOrderAjax');

